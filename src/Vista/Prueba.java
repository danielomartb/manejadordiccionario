/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Negocio.Diccionario;
import com.itextpdf.text.DocumentException;
import java.io.FileNotFoundException;


/**
 * 
 * @author Jurgenmolina <jurgenmolina29@gmail.com>
 * @author Daniel Omar Tijaro  -  Codigo: 1151859
 */
public class Prueba {
    
    public static void main(String[] args) throws FileNotFoundException, DocumentException {
        Diccionario diccionario=new Diccionario();
        diccionario.cargar("https://raw.githubusercontent.com/javierarce/palabras/master/listado-general.txt");
        //System.out.println(diccionario.getImprimir());
        //System.out.println(diccionario.buscarPalabra("aba"));
        System.out.println(diccionario.buscarPatron("abxxxxxurxxar", "uwu"));
        
        //System.out.println(diccionario.getImprimir());
        //diccionario.crearPDF(40);
        
    }
    
}
