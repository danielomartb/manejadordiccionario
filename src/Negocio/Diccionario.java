/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Palabra;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Font;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import javax.swing.JOptionPane;
import ufps.util.varios.ArchivoLeerURL;
import util.ufps.colecciones_seed.VectorGenerico;

/**
 * 
 * @author Jurgenmolina <jurgenmolina29@gmail.com>
 * @author Daniel Omar Tijaro  -  Codigo: 1151859
 */
public class Diccionario {
    
    private String urlDiccionario;
    private VectorGenerico<Palabra> palabras;
    
    /**
     * Constructor vacio
     */
    
    public Diccionario() {
    }
    
    /**
     * Carga las palabras de la URL.
     * @param urlDiccionario 
     */
    public void cargar(String urlDiccionario) {
        ArchivoLeerURL archivo=new ArchivoLeerURL("https://raw.githubusercontent.com/javierarce/palabras/master/listado-general.txt");
        Object datos[]=archivo.leerArchivo();
        //Crear los espacios:
        this.palabras=new VectorGenerico<>(datos.length);
        //índice del arreglo
        for (Object dato : datos) {
            this.palabras.add(new Palabra(dato.toString()));
            //System.out.println("se agrego una palabra" + i);
        }
    }
    /**
     * Me falta hacerlo bien
     * @param palabra_A_buscar
     * @return 
     */
    
    public boolean buscarPalabra(String palabra_A_buscar) throws Exception
    {
        if(palabra_A_buscar.isEmpty())
            throw new Exception("No se puede realizar la búsqueda patrón o matriz vacías");
        
     //Debe convertir la palabra_A_buscar a un objeto palabra:
        Palabra myPalabra_A_buscar = new Palabra(palabra_A_buscar);
        if (!palabra_A_buscar.isEmpty()) {
            try {
                for (int i = 0; i < palabras.length(); i++) {
                    if (this.palabras.get(i).compareTo(myPalabra_A_buscar) == 1) {
                        return true;
                    }
                 }
              } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Diccionario vacio");
          }
        }
      return false;
    }
    
    /**
     * Completado
     * @param patron
     * @param patron_Nuevo
     * @return 
     */
    
    public int buscarPatron(String patron, String patron_Nuevo)throws Exception
    {
        if(patron.isEmpty() || patron_Nuevo.isEmpty())
            throw new Exception("No se puede realizar la búsqueda patrón o matriz vacías");
        
        //Debe convertir el patrón a una palabra:
        Palabra myPatron=new Palabra(patron);
        Palabra myPatron_Nuevo=new Palabra(patron_Nuevo);
        /**
         * Su proceso debe realizarse con el objeto de la clase Palabra
         * myPatron y myPatron_Nuevo
         */
        int cambio = 0;
        int num=0;
        int inicioPatron=0;
        if(!patron.isEmpty() || !patron_Nuevo.isEmpty()){ // Pregunto si me enviaron datos para trabajar
            for (int i = 0; i < this.palabras.length(); i++) { // Recorrere el arreglo completo
                if(this.palabras.get(i).getCaracteres().length() >= myPatron.getCaracteres().length()){ //Quiero verificar...
                                                                                //Que no pasen palabras mas pequeñas que el patron
                    for (int j = 0; j < this.palabras.get(i).getCaracteres().length(); j++) { // lo Repetire la cantidad de caracteres que tenga
                         num = 0; // Una variable que guardara los aciertos
                        
                         for (int k = 0; k < myPatron.getCaracteres().length(); k++) { //Lo repitere solo la cantidad de caracteres que tenga el patron
                            try { // Por si se sale de rango cancelo el ciclo
                                if(this.palabras.get(i).getCaracteres().get(k+j).equals(myPatron.getCaracteres().get(k))){ 
                                  num++;
                                  
                                  /*if(num==myPatron.getCaracteres().length()){
                                    if(inicioPatron==0){
                                      inicioPatron=(k-num);
                                      cambio++;
                                    
                                    }else{
                                      inicioPatron+=(k-num)*10;        //Daniel: lo intente implementar sin el replace 
                                      cambio++;                          pero no me funciono profe, entonces lo deje que al menos
                                       }                                 me funcionara y le modificamos las mayusculas del pdf
                                                                          y las excepciones.
                                      } */                              
                                     }
                                    } catch (Exception e) {
                                      k = myPatron.getCaracteres().length();
                                }
                              }
                         if(myPatron.getCaracteres().length()==num){ //si los aciertos son iguales al numero de caracteres del patron entonces hago el cambio
                            Palabra ayuda = new Palabra(palabras.get(i).getCaracteres().toString().replace(patron, patron_Nuevo)); //una Palabra ayuda para hacer el cambio
                            palabras.set(i, ayuda); //utilizo el set de la clase vector generico
                                cambio++; // retorno el numero de cambios
                            }
                           }
                      /*if(myPatron.getCaracteres().length()==num){ //si los aciertos son iguales al numero de caracteres del patron entonces hago el cambio
                          int invertido = 0;
                          
                          for(int c = cambio; c > 0;c--){
                          invertido=invertido*10+(inicioPatron%10);
                          }
                          
                          inicioPatron=invertido;
                          
                          int tamañoArreglo = cambio*(myPatron_Nuevo.getCaracteres().capacity()- myPatron.getCaracteres().capacity());     
                        
                          VectorGenerico<Character>nuevaPalabra;
                          nuevaPalabra = new VectorGenerico<>(tamañoArreglo);
                          
                          for(int h=0;h<tamañoArreglo;h++){
                            if(inicioPatron%10==h){
                               nuevaPalabra.add(myPatron_Nuevo.getCaracteres().get(h));
                             //this.palabras.get(i).getCaracteres().set(i, nuevaPalabra.get(i));
                               }else{
                                 
                                 this.palabras.get(i).getCaracteres().set(i, nuevaPalabra.get(i));
                                }
                            nuevaPalabra.add(palabras.get(i).getCaracteres().get(h));
                            }//utilizo el set de la clase vector generico
                            myPatron.setCaracteres(nuevaPalabra);
                            
               // palabras.toString();
                       }*/
                }
                }// palabra.set(i,nuevaPalabra); 
            return cambio; 
        }
        return -1;   
    }     
          
    
    /**
     *  Retorna la información del diccionario
     * @return una cadena con la información del diccionario
     */
    public String getImprimir()
    {
        String msg = "";
        for (int i = 0; i < this.palabras.length(); i++) {
            msg += this.palabras.get(i).toString() + "\n";
        }
        return msg;
    }
    
    /**
     * 
     * @param cantidadPalabras
     * @throws FileNotFoundException
     * @throws DocumentException 
     */
     public void crearPDF(int cantidadPalabras) throws FileNotFoundException, DocumentException{
        if(cantidadPalabras <= this.palabras.length()){
            Document documento = new Document();
        
            FileOutputStream ficheroPDF = new FileOutputStream("src\\pdf\\diccionario.pdf");

            PdfWriter.getInstance(documento, ficheroPDF);

            documento.open();

            Paragraph titulo = new Paragraph("PDF creado con "+ cantidadPalabras + " palabras \n\n", FontFactory.getFont("arial", 22, Font.BOLD, BaseColor.BLACK));

            documento.add(titulo);

            PdfPTable table = new PdfPTable(1);
            table.addCell("");

            for (int i = 0; i < cantidadPalabras; i++) {
                table.addCell(this.palabras.get(i).toString().toUpperCase());
            }

            documento.add(table);


            documento.close();
        }else{
            JOptionPane.showMessageDialog(null, "No hay tantas palabras");
        }
        
    }
    
    
}
